from superpay.views import validate_signature
from superpay.models import SuperPayResponse, SuperPayFail


def superpay_registry(function):
    def wrapper(request, *args, **kwargs):
        payment_data = request.POST.dict()
        if 'merchant_id' not in payment_data:
            return JsonResponse({'status': 'error', 'message': 'Missing merchant_id parameter'})

        if 'token' not in payment_data:
            return JsonResponse({'status': 'error', 'message': 'Missing token parameter'})
        token = payment_data['token']

        if validate_signature(payment_data, token):
            kwargs.update({
                'auth': {'status': 'success', 'message': 'Successful authentication'},
                'data': payment_data
            })
            SuperPayResponse.objects.create(response=str(payment_data))
            return function(request, *args, **kwargs)
        else:
            SuperPayFail.objects.create(response=str(payment_data))
            return JsonResponse({'status': 'error', 'message': 'Unsuccessful authentication'})
    return wrapper
