import requests
from django.conf import settings


def post_payout(payload):
    response = requests.post(settings.SUPERPAY_PAYOT_URL, json=payload)
    result = response.json()
    return result
