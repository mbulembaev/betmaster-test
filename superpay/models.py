from django.db import models
from django.utils.translation import ugettext_lazy as _


class SuperPayResponse(models.Model):
    response = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Superpay response')
        verbose_name_plural = _('Superpay responses')


class SuperPayFail(models.Model):
    response = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Superpay fail')
        verbose_name_plural = _('Superpay fails')
