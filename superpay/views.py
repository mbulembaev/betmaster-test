from django.views.decorators.csrf import csrf_exempt
from superpay.decorators import superpay_registry
from superpay.send import post_payout
from wallets.models import Transaction
from django.conf import settings
from collections import OrderedDict


def generate_signature(data):
    """ Generate signature by using requested data
     and secret key given to us from SuperPay
    """

    "Signature generation logic"

    return "generated_signature"


def validate_signature(data, token):
    """ Validate signature by using requested data
     and secret key given to us from SuperPay
    """

    "Logic to generate expected_token by using data"

    return expected_token == token


@csrf_exempt
@superpay_registry
def superpay_callback(request, auth, data):
    transaction_id = data['order_id']
    try:
        transaction = Transaction.objects.get(id=transaction_id)
        transaction.complete()
        return JsonResponse({'status': 'success', 'message': 'Transaction completed'})
    except Transaction.DoesNotExist:
        return JsonResponse({'status': 'error', 'message': 'Unknown Transaction'})


def superpay_deposit_form(order_id, order_date, currency, amount, description, success_url, fail_url, locale):
    data = OrderedDict([
        ("merchant_id", settings.SUPERPAY_CLIENT_ID),
        ("order_id", order_id),
        ("order_date", order_date.strftime('%Y-%m-%d %H:%M:%S')),
        ("currency", currency),
        ("amount", amount),
        ("description", description),
        ("success_url", success_url),
        ("fail_url", fail_url),
        ("locale", locale),
    ])
    signature = generate_signature(data)
    items = []
    for key, val in data.items():
        items.append(f'{key}:<input name="{key}" value="{val}" type="text">')
    items.append(f'<input name="token" value="{signature}" type="hidden">')

    return '<form action="{0}" method="post" id="payment_form">{1}<input type="submit"></form>'.format(
        settings.SUPERPAY_DEPOSIT_URL, '<br>\n'.join(items))


def superpay_payout(order_id, order_date, currency, amount, description, wallet_id):
    data = OrderedDict([
        ("merchant_id", settings.SUPERPAY_CLIENT_ID),
        ("order_id", order_id),
        ("order_date", order_date.strftime('%Y-%m-%d %H:%M:%S')),
        ("currency", currency),
        ("amount", amount),
        ("description", description),
        ("wallet_id", wallet_id),
    ])
    signature = generate_signature(data)
    data['token'] = signature
    set_task(
        post_payout,
        payload=data,
    )
    return JsonResponse({'status': 'success', 'message': 'Payout request sent'})
