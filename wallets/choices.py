from django.utils.translation import ugettext_lazy as _

DEPOSIT = 'deposit'
WITHDRAW = 'withdraw'

TRANSACTION_TYPES = (
    (DEPOSIT, _('Deposit')),
    (WITHDRAW, _('Withdraw'))
)
