from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from superpay.views import superpay_deposit_form, superpay_payout


@csrf_exempt
@login_required
def deposit_amount(request):
    data = request.POST.dict()
    amount = data.get('amount')
    currency = data.get('currency')
    description = data.get('description')
    success_url = data.get('success_url')
    fail_url = data.get('fail_url')
    locale = data.get('locale')
    user = request.user

    transaction = user.wallet.deposit(amount, currency, description, success_url, fail_url)

    form = superpay_deposit_form(transaction.id, transaction.created, currency, amount,
                                 description, success_url, fail_url, locale)
    return render(request, 'payment_form.html', {"form": form})


@csrf_exempt
@login_required
def withdraw_amount(request):
    data = request.POST.dict()
    amount = data.get('amount')
    currency = data.get('currency')
    description = data.get('description')
    wallet_id = data.get('wallet_id')
    user = request.user

    transaction = user.wallet.withdraw(amount, currency, description)
    payout = superpay_payout(transaction.id, transaction.created, currency, amount, description, wallet_id)
    return payout
