from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from decimal import Decimal
from django.db.models import Sum
from wallets.choices import TRANSACTION_TYPES, DEPOSIT, WITHDRAW
from django.db import transaction
from django.db.models import Q


class Wallet(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.PROTECT)
    currency = models.CharField(_('Currency'), max_length=3)
    balance = models.DecimalField(_('Balance'), max_digits=12, decimal_places=2, default=0)
    holded = models.DecimalField(_('Holded'), max_digits=12, decimal_places=2, default=0)

    class Meta:
        verbose_name = _('Wallet')
        verbose_name_plural = _('Wallets')

    def __str__(self):
        return f'{self.user} {self.balance} {self.holded} {self.currency}'

    def calculate_balance(self, save=False):
        with transaction.atomic():
            calc = Transaction.objects.filter(
                Q(wallet=self, is_completed=True, is_holded=False) or
                Q(wallet=self, is_completed=False, is_holded=True)
            ).aggregate(
                balance=Sum('balance'),
                holded=Sum('holded'),
            )
            for key, value in calc.items():
                if value is None:
                    calc[key] = Decimal('0')
            if save:
                self.balance = calc['balance']
                self.holded = calc['holded']
                self.save()
        return calc

    def deposit(self, amount, currency, description, success_url, fail_url):
        """ Deposit an amount to the wallet by
        creating a new transaction with the deposit amount.
        """
        if not isinstance(amount, int) and not isinstance(amount, Decimal):
            raise ValueError("Value must be int or Decimal")
        if amount < 0:
            raise ValueError("Can't deposit a negative amount")

        with transaction.atomic():
            deposit_transaction = self.transactions.create(
                type=DEPOSIT,
                amount=amount,
                currency=currency,
                description=description,
                success_url=success_url,
                fail_url=fail_url
            )
        return deposit_transaction

    def withdraw(self, amount, currency, description):
        """ Withdraw an amount from the wallet by
        creating a new transaction with the withdrawal amount.
        """
        if not isinstance(amount, int) and not isinstance(amount, Decimal):
            raise ValueError("Value must be int or Decimal")
        if amount < 0:
            raise ValueError("Can't withdraw a negative amount")
        if amount > self.balance:
            raise ValueError("Insufficient balance")

        with transaction.atomic():
            payout_transaction = self.transactions.create(
                type=WITHDRAW,
                amount=amount * Decimal('-1.0'),
                holded=amount,
                is_holded=True,
                currency=currency,
                description=description
            )

        self.calculate_balance(save=True)

        return payout_transaction

    def unhold(self, transaction):
        """ Unhold an amount on the wallet by
        updating a transaction, when payout is done.
        """
        holded_amount = transaction.holded

        if not isinstance(holded_amount, int) and not isinstance(holded_amount, Decimal):
            raise ValueError("Value must be int or Decimal")
        if holded_amount < 0:
            raise ValueError("Can't unhold a negative amount")
        if holded_amount > self.holded:
            raise ValueError("Insufficient balance")

        with transaction.atomic():
            transaction.is_holded = False
            transaction.save()

        return transaction


class Transaction(models.Model):
    wallet = models.ForeignKey(Wallet, on_delete=models.PROTECT, related_name='transactions')
    type = models.CharField(_('Type'), choices=TRANSACTION_TYPES, max_length=10)
    created = models.DateTimeField(_('Created'), auto_now_add=True)
    updated = models.DateTimeField(_('Updated'), auto_now=True)
    amount = models.DecimalField(_('Amount'), max_digits=12, decimal_places=2, default=0)
    holded = models.DecimalField(_('Holded'), max_digits=12, decimal_places=2, default=0)
    currency = models.CharField(_('Currency'), max_length=3)
    description = models.CharField(_('Description'), max_length=100, blank=True, null=True)
    success_url = models.URLField(_('Success url'), blank=True, null=True)
    fail_url = models.URLField(_('Fail url'), blank=True, null=True)
    is_completed = models.BooleanField(_('Is completed'), default=False)
    is_holded = models.BooleanField(_('Is holded'), default=False)

    class Meta:
        verbose_name = _('Transaction')
        verbose_name_plural = _('Transactions')

    def __str__(self):
        return f'{self.amount} {self.holded} {self.currency}'

    def complete(self):
        if not self.is_completed:
            with transaction.atomic():
                self.is_completed = True
                self.save()
            if self.type == WITHDRAW:
                self.wallet.unhold(self)
            self.wallet.calculate_balance(save=True)
