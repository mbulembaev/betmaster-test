import json
import uuid
from django.urls import reverse
import pytest
from wallets.choices import DEPOSIT


@pytest.fixture
def password():
    return 'some-password'


@pytest.fixture
def create_user(db, django_user_model, password):
    def make_user(**kwargs):
        kwargs['password'] = password
        if 'username' not in kwargs:
            kwargs['username'] = str(uuid.uuid4())
        return django_user_model.objects.create_user(**kwargs)

    return make_user


@pytest.fixture
def login_user(db, client, create_user, password):
    def make_login(user=None):
        if user is None:
            user = create_user()
        client.login(username=user.username, password=password)
        return client, user

    return make_login


@pytest.fixture
def add_funds(db, wallet):
    def create_transaction(**kwargs):
        return wallet.transactions.create(
                type=DEPOSIT,
                amount=100,
                currency='USD',
                description='Test deposit transaction',
                is_completed=True
            )

    return create_transaction


@pytest.mark.django_db
def test_deposit_amount(login_user):
    client, user = login_user()
    url = reverse('deposit-amount-url')
    data = {
        'amount': 100,
        'currency': 'USD',
        'description': "Deposit amount",
        'success_url': 'https://example.com/superpay?result=success',
        'fail_url': 'https://example.com/superpay?result=failure',
        'locale': 'en'
    }
    response = client.post(url, data=json.dumps(data))
    assert response.status_code == 201
    assert 'payment_form' in response.content


@pytest.mark.django_db
def test_withdraw_amount(login_user, add_funds):
    client, user = login_user()
    add_funds(user.wallet)
    url = reverse('withdraw-amount-url')
    data = {
        'amount': 50,
        'currency': 'USD',
        'description': "Withdraw amount",
        'wallet_id': 1234567
    }
    response = client.post(url, data=json.dumps(data))
    json_data = json.loads(response.data)
    assert response.status_code == 201
    assert 'status' in json_data['status']
    assert 'message' in json_data['message']
    assert data["status"] == "success"
    assert data["message"] == "Payout request sent"
