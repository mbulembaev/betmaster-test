from django.conf.urls import url
from wallets import views

urlpatterns = [
    url(r'^deposit/$', view=views.deposit_amount, name="deposit-amount-url"),
    url(r'^withdraw/$', view=views.withdraw_amount, name="withdraw-amount-url"),
]
